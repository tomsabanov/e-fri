var cheerio = require('cheerio');

var rp = require('request-promise').defaults({jar:true});
var auth =require('./authentication').auth;

function getCourses(){
  return auth
  .then(function(response){
    return getMyCourses();
  })
  .then(function(courses_body){
    var temp_courses = extractCoursesFromBody(courses_body);
    return temp_courses;
  })
  .catch(function(error){
    console.log(error);
  })
}

function extractCoursesFromBody($){
  var courses = {};
  $('div[class="course_list"]').find('div[class="box coursebox"]').each(function (index, element) {
    var courseObject = courseElementToObject(element);
    courses[courseObject.courseId] = Object.assign({}, courseObject);
  });
  return courses;
}

function courseElementToObject(el){
  var url = 'https://ucilnica.fri.uni-lj.si/course/view.php?id='+el.attribs.id.replace( /^\D+/g, '');
  return{
    courseId : el.attribs.id.replace( /^\D+/g, ''),
    courseLink:url,
    courseName : el.children[0].children[0].children[0].children[0].data,
    courseData : getCourseData(url)
  }
}
function getCourseData(url){
  return rp(getOptions(url))
  .then(function($){
    var section_0 = $('li[id="section-0"]').find('div[class="content"]');
    var summary = ";"


    console.log($.html());
    //console.log(section_0.html())


    $('div[class="course_list"]').find('div[class="box coursebox"]').each(function (index, element) {
      var courseObject = courseElementToObject(element);
      courses[courseObject.courseId] = Object.assign({}, courseObject);
    });

  })
}

function getCurrentMonthEvents(){
  var url ='https://ucilnica.fri.uni-lj.si/calendar/view.php?view=month';
  return rp(getOptions(url));
}
function getMyCourses(){
  var url ='https://ucilnica.fri.uni-lj.si/my/';
  return rp(getOptions(url));
}

function getOptions(url){
  return {
    uri: url,
    transform: function (body) {
        return cheerio.load(body);
    }
  }
}

module.exports = {getCourses}
