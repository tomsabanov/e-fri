
import React, { Component } from 'react';

import Login from './app/Login/Login';
import Main from './app/Main';

import { StackNavigator } from 'react-navigation';

const RootStack = StackNavigator(
  {
    'Login': {
      screen: Login,
      header:null
    },
    'Main':{
      screen:Main,
      header:null
    }
  },
  {
    initialRouteName: 'Login',
    headerMode:'none'
  },
);

export default class App extends Component{
  render() {
    return <RootStack />;
  }
}
