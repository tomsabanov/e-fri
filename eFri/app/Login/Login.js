import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  ImageBackground
} from 'react-native';

import LoginForm from './LoginForm'

import { Col, Row, Grid } from "react-native-easy-grid";

export default class Login extends Component{
  navigate(){
    this.props.navigation.navigate('Main')
  }
  render() {
    return (
      <ImageBackground
        style={{height:'100%', width:'100%', zIndex:1}}
        source={require('./bg.png')}
        resizeMode = 'cover'
      >
        <Grid>
          <Row size={30}>
            <Image
              style={{height:100, width:100,top:'20%',left:'35%',position:'absolute'}}
              source={require('./logo.png')}
            />
          </Row>
          <Row size={70}>
            <LoginForm navigate={this.navigate.bind(this)}/>
          </Row>
        </Grid>
      </ImageBackground>
    );
  }
}
