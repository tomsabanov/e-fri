import React, { Component } from 'react';

import {
  View
} from 'react-native';

import { Form, Item, Input, Footer } from 'native-base';

import { Container, Content, Card, CardItem, Body, Text, Button, Label } from 'native-base';

import { Col, Row, Grid } from "react-native-easy-grid";


export default class LoginForm extends Component {
  render() {
    return (
      <View style={{width:'80%', height:'100%', left:'5%',top:'-10%'}}>
          <Container>
            <Content>
                        <Form>
                          <Item floatingLabel>
                            <Label style={{color:'white'}}>Email</Label>
                            <Input style={{color:'white'}} />
                          </Item>
                          <Item floatingLabel>
                            <Label style={{color:'white'}}>Geslo</Label>
                            <Input style={{color:'white'}} />
                          </Item>
                        </Form>
                        <View style={{width:'100%',justifyContent: 'center', alignItems: 'center'}}>
                          <Button style={{marginLeft:'37%',marginTop:40,backgroundColor:'#e12a26', position:'relative'}} onPress={this.props.navigate}>
                              <Text>Prijava</Text>
                          </Button>
                        </View>
            </Content>
          </Container>
      </View>
    );
  }
}
/*


                        <Button style={{left:'85%', backgroundColor:'#e12a26'}} onPress={this.props.navigate}>
                          <Text> Prijavi se! </Text>
                        </Button>

                        */
