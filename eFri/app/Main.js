import React, { Component } from 'react';
import Sidebar from './Sidebar/Sidebar';

import {Text, View} from 'react-native'

import Drawer from 'react-native-drawer'

export default class DrawerExample extends Component {
  closeDrawer(){
    this.drawer._root.close()
  }
  openDrawer(){
    this.drawer._root.open()
  }
  render() {
    return (
      <Drawer
        type="overlay"
        content={<Sidebar />}
        tapToClose={true}
        panCloseMask={0.8}
        panOpenMask={0.3}
        openDrawerOffset={0.4}
        closedDrawerOffset={-3}
        >
          <Text>Go</Text>
      </Drawer>
    );
  }
}
