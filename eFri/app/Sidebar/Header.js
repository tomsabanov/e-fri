import React, {Component} from 'react';

import {View, Text, Image} from 'react-native';

export default class Header extends Component{

  render(){
    return(

      <View style={{height: '100%',width:'100%'}}>
        <View style={{alignItems:'center',width:'100%',  borderBottomColor: 'white',
borderBottomWidth: 1,top:40, paddingBottom:10}}>
          <Image style={{width:50,height:50,}} source={require('../Login/logo.png')}/>
          <Text style={{color:'white'}}>Janez Novak</Text>
        </View>

        <View style={{alignItems:'center',width:'100%',  borderBottomColor: 'white',
borderBottomWidth: 1,top:50,paddingBottom:10}}>
          <Text style={{color:'white'}}>ts2659@student.uni-lj.si</Text>
          <Text style={{color:'white'}}>63170000</Text>
        </View>

      </View>
    )
  }


}
