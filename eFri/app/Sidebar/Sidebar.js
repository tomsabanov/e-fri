import React, { Component } from 'react';
import {
  Text,
  View,
  Image
} from 'react-native';

import { Col, Row, Grid } from "react-native-easy-grid";

import Header from './Header'


export default class Sidebar extends Component{
  render() {
    return (
      <View style={{backgroundColor: '#323232', position:'absolute',
                    height:'100%', width:'100%'
                  }}>
                  <View style={{width:'90%', left:'4%',height:'90%'}}>
                    <Grid>
                      <Row size={1}>
                        <Header/>
                      </Row>
                      <Row size={3}>
                        <Text>smthing</Text>
                      </Row>
                    </Grid>
                  </View>
      </View>
    );
  }
}
