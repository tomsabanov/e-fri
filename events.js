var cheerio = require('cheerio');

var rp = require('request-promise').defaults({jar:true});

var auth =require('./authentication').auth;

function getEvents(){
    return auth
    .then(function(response){
        return getCurrentMonthEvents()
    })
    .then(function($){
      return getUrlOfDays($)
    })
    .then(function(url){
      var day_promises = buildDayPromises(url);
      return Promise.all(day_promises);
    })
    .then(function(results){
      var temp_events = [];
      results.forEach(function($){
        temp_events = temp_events.concat(extractEventsFromDayBody($));
      })
      return temp_events;
    })
}
function getCurrentMonthEvents(){
  var options = {
    uri: 'https://ucilnica.fri.uni-lj.si/calendar/view.php?view=month',
    transform: function (body) {
        return cheerio.load(body);
    },
  };
  return rp(options);
}

function getUrlOfDays($){
    var url = [];
    var i =0;
    $('div[class="day"]').find('a').each(function (index, element) {
      url[i] = element.attribs.href;
      i++;
    });
    return url;
}

function buildDayPromises(url){
  var promises = [];
  for(var i = 0; i<url.length;i++){
    promises[i] = getDayEvents(url[i]);
  }
  return promises;
}
function getDayEvents(element){
  var options = {
    uri: element,
    transform: function (body) {
        return cheerio.load(body);
    }
  };
  return rp(options);
}
function extractEventsFromDayBody($){
  var events = [];
  var date = "";
  var courseId="";
  var eventName="";

  $('span[class=current]').each(function (index, element) {
     date=element.children[0].data;
  });
  var i = 0;
  $('div[class="event"]').each(function (index, element) {
    var eventID = element.attribs.id;
    var id = ""
    $('div[id='+eventID+']').find('div[class="course"] a').each(function (index, element) {
       id = element.attribs.href.replace( /^\D+/g, '');
    });
    $('div[id='+eventID+']').find('h3 a').each(function (index, element) {
       eventName = element.children[0].data;
    });
    var eventDescription = "";
    $('div[class="description"]').find('p').each(function(index, element){
	eventDescription+= element.children[0].data;
    });

    var b = true;
    for(var l = 0; l<events.length;l++){
      if(events[l].courseId == id){
        events[l].events.push(eventName);
        b = false;
      }
    }
    if(b==true){
      events.push({
        courseId : id,
        events : [eventName],
        date: date,
	eventDescription,
      })
    }
    i++;
  });
  return events;
}
module.exports = {getEvents}
