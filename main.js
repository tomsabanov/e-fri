var express = require('express');
var app = express();



var getCourses = require('./courses.js').getCourses;
/*var courses = getCourses().then(function(res){
  console.log(res);
});
*/

var getEvents = require('./events.js').getEvents;
/*var events = getEvents(false).then(function(res){
  console.log(res)
})
*/
var getSchedule = require('./schedule.js').getSchedule;
/*var schedule = getSchedule()
  .then(function(res){
	if(res != undefined){
		console.log(res.html());

	}
   })
*/

getCourses()
.then(getEvents())
.then(getSchedule())
.then(function(data){
  console.log(data);
})


app.get('/', function (req, res) {
  res.send('Hello world');
})

app.listen(3000, function () {
  console.log('Listenint on port 3000.');
});
