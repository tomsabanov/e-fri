import requests;
from bs4 import BeautifulSoup

class Main:
    def main(self):
        student=Student("ts2659@student.uni-lj.si","babo211megli17")
        payload = {
            'username' : 'ts2659@student.uni-lj.si',
            'password' : 'babo211megli17'
        }
        student.setData("https://ucilnica.fri.uni-lj.si/login/index.php",payload)

class Student:
    def __init__(self,username,password):
        self.username=username
        self.password=password
        self.courses = None
    def setData(self,login_url, payload):
        self.session = requests.Session()
        self.session.post(login_url, data=payload)
        self.setCourses("https://ucilnica.fri.uni-lj.si/my/",self.session)
        self.setEvents("https://ucilnica.fri.uni-lj.si/calendar/view.php?view=month&course=1", self.session)
    def setCourses(self,url,session):
        student_courses={}
        r = session.get(url)
        soup = BeautifulSoup(r.text, 'html.parser')
        courses = soup.find_all('div', class_='coursebox')

        for course in courses:
            course_id = course.attrs['id']
            course_id = ''.join(i for i in course_id if i.isdigit())
            course_title = course.find('h2', class_='title').text
            student_courses[course_id] = Course(course_id,course_title)
            print(course_id  + " " + course_title)
        self.courses = student_courses
    def setEvents(self,url,session):
        #Gremo na koledar, dobimo dneve, url teh dnevov, in gremo potem se na vsak dobljeni dan, kjer dobimo dogodke...
        #Moramo tudi ko dobimo koledar, setat tudi v events mesec, tja se bodo potem limali eventi....
        r = session.get(url)
        soup = BeautifulSoup(r.text,'html.parser')
        days = soup.find_all('div', class_='day')
        for day in days:
            a = day.find("a")
            if(hasattr(a, 'attrs')):
                    day=session.get(a['href'])
                    soup=BeautifulSoup(day.text, 'html.parser')
                    events = soup.find_all('div',class_='event')
                    day_date = soup.find('span', class_='current').text
                    for event in events:
                        try:
                            payload = {
                                'event_type' : event.img.attrs['title'],
                                'event_name' : event.h3.a.text,
                                'event_course_id' : event.div.a.attrs['href'].split('id=')[-1],
                                'event_course_name' : event.div.a.text,
                                'event_time' : event.span.text,
                            }
                            self.addEventToCourse(payload)
                        except KeyError:
                            #Ce pride do problema, potem je to verjetno izpit/kolokvij... -> TO BO TREBA BOLS NAREDIT.... VERJETNO??? KAJ CE JE KAJ DRUGEGA?????????
                            payload = {
                                    'event_type' : "Kolokvij/Izpit",
                                    'event_name': event.h3.text,
                                    'event_course_id' : event.div.a.attrs['href'].split('id=')[-1],
                                    'event_course_name' : event.div.a.text,
                                    'event_time' : event.span.text,
                            }
                            self.addEventToCourse(payload)
                        #print(payload['event_course_id'])
    def addEventToCourse(self, payload):
        self.courses[payload['event_course_id']] = payload
        print(self.courses[payload['event_course_id']])

class Course:
    def __init__(self,id,name,link=None,events=None):
        self.id = id
        self.name = name
        self.link = link
        self.events = events
class Event:
    def __init__(self,id,course_id,name,course_name,type,time,date,description):
        self.id = id
        self.course_id = course_id
        self.course_name = course_name
        self.name = name
        self.type = type
        self.info = info
        self.time = time
        self.date = date

if __name__ == '__main__':
    main=Main()
    main.main()
