var rp = require('request-promise');
var cheerio = require('cheerio');


function getSchedule(){
  return getScheduleBody()
    .then(function(res){
      return extractDataFromSchedule(res)
    })
}

function getScheduleBody(){
  var options = {
    uri:'https://urnik.fri.uni-lj.si/timetable/fri-2017_2018-letni-1-teden/allocations?student=63170272',
    transform: function(body){
      return cheerio.load(body)
    }
  };
  return rp(options);
}

function extractDataFromSchedule($){
  var schedule = {
    MON:[],
    TUE:[],
    WED:[],
    THU:[],
    FRI:[]
  }

  $('table[class="time_preferences"]').find('tr[class="timetable"]').each(function(index, element){
      $(this).find('td').each(function(i, el){
          if(el.attribs.class.includes('allocated')){
            var text = $(this).find('span').text();

            var time = text.substring(text.indexOf(':')-2,text.indexOf(':') + 11);
            var additional_info = text;
            var day = el.attribs.class.substring(0,3);

            var subject = $(this).find('a[class="activity"]').text();
            var classroom = $(this).find('a[class="classroom"]').text()
            var teachers = [];
            $(this).find('a[class="teacher"]').each(function(i,el){
              teachers.push(el.children[0].data);
            })

            var obj = {day,time, subject, classroom, teachers, additional_info};
            schedule[day].push(obj);
          }
      })
  })
  console.log(schedule)

}

module.exports = {getSchedule};
